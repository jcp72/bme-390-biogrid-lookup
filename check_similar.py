import requests, json
from typing import List
from urllib import parse

underscorify = lambda s: s.replace(" ", "_")

class Result():

    def __init__(self, res: dict):
        self.scientificName = res["organism"]["scientificName"]
        self.commonName = (
            res["organism"]["commonName"] 
            if "commonName" in res["organism"] else ""
        )
        self.exist = res["proteinExistence"]
        self.sequence = res["sequence"]["value"]
        self.id = res["uniProtkbId"]

def main():

    ligand90 = "https://rest.uniprot.org/uniprotkb/search?facets=reviewed%2Cmodel_organism%2Cproteins_with%2Cexistence%2Cannotation_score%2Clength&fields=id%2Creviewed%2Corganism_name%2Cprotein_name%2Csequence&query=%28%28uniref_cluster_90%3AUniRef90_P03971%29%20AND%20NOT%20%28accession%3AP03971%29%29&size=10"
    ligand50 = "https://rest.uniprot.org/uniprotkb/search?fields=organism_name,sequence,id,protein_existence&query=((uniref_cluster_50:UniRef50_P03971)%20AND%20NOT%20(accession:P03971))&size=500"

    receptor50 = "https://rest.uniprot.org/uniprotkb/search?fields=organism_name,sequence,id,protein_existence&query=(uniref_cluster_50:UniRef50_Q16671%20OR%20uniref_cluster_50:UniRef50_Q16671-2)&size=500"

    URL = ligand50

    similar = json.loads(requests.get(URL).text)

    results: List[Result] = []

    for res in similar["results"]:
        r = Result(res)
        # if r.commonName != "Crab-eating macaque":
        #     results.append(r)
        results.append(r)

    l = [f'{i.exist}, {i.commonName}' for i in results]; l.sort()
    for i in l: print(i)

    # open("sim_seq.txt", "w").write("\n".join([
    #     f"> {r.id}|{underscorify(r.commonName)}\n" + r.sequence for r in results]
    # ))

if __name__ == "__main__":
    main()
