import json, requests
from check_similar import Result, underscorify

def main():

    get_not_amhr2 = lambda f: (f[0], f[4]) if f[0] != "106766" else (f[1], f[5])

    interact_full = [
        l.split("\t")
        for l in open("interact.tsv", "r").read().splitlines()[1:]
    ]

    interact_list = [get_not_amhr2(l[3:9]) for l in interact_full]

    URL = lambda number: f"https://rest.uniprot.org/uniprotkb/search?fields=organism_name,sequence,id,protein_existence&query=(xref:biogrid-{number})&size=1"

    results: list[Result] = []

    for pid, name in interact_list:
        interact = json.loads(requests.get(URL(pid)).text)
        try:
            results.append(Result(interact["results"][0]))
        except:
            print(f"WARNING: Error when processing {name}...")

    open("interact_seq.txt", "w").write("\n".join([
        f"> {r.id}|{underscorify(r.commonName)}\n" + r.sequence for r in results]
    ))

if __name__ == "__main__":
    main()
