
import re

regexp = "^([^\\s]+\\s+)([^\\s]+)"
file = "receptor_msa_res.txt"
# file = "ligand_msa_res.txt"

seqs = {}

lines = open(file, "r").read().splitlines()[3:]

for line in lines:
    s = re.search(regexp, line)
    if s:
        g = s.groups()
        name = g[0]
        seq = g[1]
        if name not in seqs:
            seqs[name] = ""
        seqs[name] += seq

seqs_list = [[f"{k}{seqs[k]}", len(seqs[k].replace("-", ""))] for k in seqs]

seqs_list.sort(key = lambda v: v[1])

open(file.replace("res", "comp"), 
    "w").write("\n".join([f"{s[1]:4d} {s[0]} {s[1]:4d}" for s in seqs_list]))
